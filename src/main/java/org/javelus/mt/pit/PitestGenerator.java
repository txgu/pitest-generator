package org.javelus.mt.pit;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.pitest.classinfo.ClassName;
import org.pitest.classpath.ClassPath;
import org.pitest.classpath.ClassPathByteArraySource;
import org.pitest.functional.predicate.False;
import org.pitest.mutationtest.engine.Mutant;
import org.pitest.mutationtest.engine.Mutater;
import org.pitest.mutationtest.engine.MutationDetails;
import org.pitest.mutationtest.engine.MutationEngine;
import org.pitest.mutationtest.engine.MutationIdentifier;
import org.pitest.mutationtest.engine.gregor.config.GregorEngineFactory;
import org.pitest.mutationtest.engine.gregor.config.Mutator;

public class PitestGenerator {
    static File outputFolder;
    static String classPathString;
    static ClassPath classPath;
    static Set<String> includeClasses = new HashSet<String>();
    static ClassPath createClassPath(String classPathString, boolean includeRT) {
        String[] paths = classPathString.split(File.pathSeparator);
        List<File> files = new ArrayList<File>();
        for (String p : paths) {
            files.add(new File(p));
        }
        if (includeRT) {
            String rtPath = getRTPath();
            if (rtPath != null) {
                files.add(new File(rtPath));
            }
        }
        return new ClassPath(files);
    }
    
    static String getRTPath() {
        String fileName = String.class.getResource("String.class")
                .toExternalForm();
        Matcher matcher = Pattern.compile("jar:(file:/.*)!.*")
                .matcher(fileName);
        if (matcher.matches()) {
            String path = matcher.group(1);

            // add the root of jar to the path list.
            URI uri;
            try {
                uri = new URI(path);
                uri.toURL();
                return uri.getPath();
            } catch (URISyntaxException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        return null;
    }

    static void processCommandline(String[] args) {
        int i = 0;

        while (i < args.length) {
            String option = args[i++];
            if (option.equals("-cp")) {
                classPathString =args[i++];
            } else if (option.equals("-class-list")) {
                parseClassList(args[i++]);
            } else if (option.equals("-o")) {
                outputFolder = new File(args[i++]);
            } else {
                throw new IllegalArgumentException("Unknown options.");
            }
        }

        if (classPathString == null) {
            throw new IllegalArgumentException("-cp should not be null");
        }

        if (outputFolder == null) {
            outputFolder = new File(System.getProperty("pitest.writeMutantsFolder", "pitest-mutants"));
        }
        if (!outputFolder.exists()) {
            outputFolder.mkdirs();
        }
        
        if (includeClasses.isEmpty()) {
            ClassPath path = createClassPath(classPathString, false);
            includeClasses.addAll(path.classNames());
            classPath = createClassPath(classPathString, true);
        } else {
            classPath = createClassPath(classPathString, true);
        }
    }

    private static void parseClassList(String string) {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(string));
            String line = null;
            while ((line = br.readLine()) != null) {
                includeClasses.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String [] args) {
        processCommandline(args);
        ClassPathByteArraySource source = new ClassPathByteArraySource(classPath);
        GregorEngineFactory gregorEngineFactory = new GregorEngineFactory();
        MutationEngine engine = gregorEngineFactory.createEngineWithMutators(False.<String>instance(), Mutator.all());
        Mutater mutater = engine.createMutator(source);
        File logFile = new File(outputFolder, "mutants.log");
        BufferedWriter logWriter = null;
        int totalCount = 0;
        try {
            logWriter = new BufferedWriter(new FileWriter(logFile));
            outer: for (String cls : includeClasses) {
                ClassName clsName = ClassName.fromString(cls);
                System.out.println(">>> Generating mutants for " + clsName.asJavaName());
                int count = 0;
                for (MutationDetails md : mutater.findMutations(clsName)) {
                    MutationIdentifier id = md.getId();
                    Mutant mutant = null;
                    try {
                        mutant = mutater.getMutation(id);
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                        continue outer;
                    }
                    count ++;
                    totalCount++;
                    System.out.print(".");
                    //mutationIdToFolder(id);
                    String mutantFolder = String.valueOf(totalCount); // String.format("mutatant-%d", totalCount);
                    logWriter.write(String.format("%s %s\n", totalCount, md.toString()));
                    byte[] wbytes = mutant.getBytes();
                    String fileName = id.getClassName().asInternalName() + ".class";
                    File file = new File(new File(outputFolder, mutantFolder), fileName);
                    File parent = file.getParentFile();
                    if (!parent.exists()) {
                        parent.mkdirs();
                    }
                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(file);
                        fos.write(wbytes);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        if (fos != null) {
                            try {
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                System.out.println(count);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (logWriter != null) {
                try {
                    logWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
